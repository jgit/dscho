#!/bin/sh

cd "$(dirname "$0")" &&

if test ! -f jgit.jar
then
	./make_jgit.sh
fi &&

cd graphgui &&
javac -cp ../jgit.jar *.java
