#!/bin/sh

cd "$(dirname "$0")"/graphgui &&

CLASS=BasicGUI
if test ! -f $CLASS.class
then
	../make-graphgui.sh
fi &&

java -cp ../jgit.jar:. $CLASS
