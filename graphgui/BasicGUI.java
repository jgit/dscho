import javax.swing.SwingUtilities;
import javax.swing.JFrame;

public class BasicGUI {

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI(); 
			}
		});
	}

	private static void createAndShowGUI() {
		JFrame f = new JFrame("Basic GUI #1 - Vertical string of shapes");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		f.add(new CommitGraphPanel());
		f.setSize(400,625);
		f.setVisible(true);
	}

}
