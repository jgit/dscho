import java.io.File;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Graphics; 
import java.awt.Graphics2D;
import java.awt.GradientPaint;
import javax.swing.JPanel;
import javax.swing.BorderFactory;

import org.spearce.jgit.lib.Repository;
import org.spearce.jgit.revwalk.RevSort;

class CommitGraphPanel extends JPanel {

	final int COMMIT_COUNT = 10;

	protected GraphCommit[] commits = new GraphCommit[COMMIT_COUNT];

	public CommitGraphPanel() {
		setBorder(BorderFactory.createLineBorder(Color.black));
		readCommits();
	}

	public Dimension getPreferredSize() {
		return new Dimension(200, 600);
	}

	public void readCommits() {
		try {
			Repository db = new Repository(new File("../.git"));

			GraphWalk gw = new GraphWalk(db);
			gw.markStart(gw.parseCommit(db.resolve("HEAD")));
			gw.sort(RevSort.COMMIT_TIME_DESC, true);

			for(int i = 0; i < COMMIT_COUNT; i++) {
				commits[i] = (GraphCommit) gw.next();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void paintComponent(Graphics g) {
		CommitPainter cp = new CommitPainter( (Graphics2D) g);
		for(int i = 0; i < COMMIT_COUNT; i++) {
			if(commits[i] != null) {
				cp.paintCommit(commits[i]);
			}
		}
	}
}
