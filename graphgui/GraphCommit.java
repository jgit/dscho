import org.spearce.jgit.lib.AnyObjectId;
import org.spearce.jgit.revwalk.RevCommit;

public class GraphCommit extends RevCommit {

	protected int xPos = 0;
	protected int yPos = 0;
	protected int cumulatedX = 0;
	protected int childCount = 0;

	protected GraphCommit(final AnyObjectId id) {
		super(id);
	}

	public int getXPos() {
		return xPos;
	}

	public void setXPos(int xPos) {
		this.xPos = xPos;
	}

	public int getYPos() {
		return yPos;
	}

	public void setYPos(int yPos) {
		this.yPos = yPos;
	}

	public int getCumulatedX() {
		return cumulatedX;
	}

	public void setCumulatedX(int x) {
		cumulatedX = x;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}
}
