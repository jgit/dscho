import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.GradientPaint;

class CommitPainter {

	protected Graphics2D g2d;

	public CommitPainter(Graphics2D g2d) {
		this.g2d = g2d;
	}

	public void paintCommit(GraphCommit c) {
		final int HEIGHT = 30;
		final int WIDTH = 200;
		final int SPACING = 20;

		int xPos = c.getXPos() * (WIDTH+SPACING);
		int yPos = c.getYPos() * (HEIGHT+SPACING);

		Color color1 = Color.LIGHT_GRAY;
		Color color2 = color1.darker();

		g2d.setClip(new Rectangle(xPos, yPos, xPos + WIDTH, yPos + HEIGHT));

		GradientPaint gp = new GradientPaint(
			xPos, yPos, color1,
			xPos, yPos + HEIGHT, color2);

		g2d.setPaint(gp);
		g2d.fillRect(xPos, yPos, WIDTH, HEIGHT);
		g2d.setColor(Color.GRAY);
		g2d.drawRect(xPos, yPos, WIDTH, HEIGHT);

		g2d.setColor(Color.BLACK);
		System.out.println(c.getShortMessage());
		g2d.drawString(c.getShortMessage(), xPos, yPos+HEIGHT/2);
	}
}
