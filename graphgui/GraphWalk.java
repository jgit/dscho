import java.io.IOException;

import org.spearce.jgit.lib.AnyObjectId;
import org.spearce.jgit.lib.Repository;
import org.spearce.jgit.revwalk.RevWalk;
import org.spearce.jgit.revwalk.RevCommit;
import org.spearce.jgit.errors.IncorrectObjectTypeException;
import org.spearce.jgit.errors.MissingObjectException;

public class GraphWalk extends RevWalk {
	public GraphWalk(final Repository repo) {
		super(repo);
	}

	protected RevCommit createCommit(final AnyObjectId id) {
		return new GraphCommit(id);
	}

	public RevCommit next() throws MissingObjectException,
			IncorrectObjectTypeException, IOException {
		GraphCommit c = (GraphCommit) super.next();
		GraphCommit p;

		// Update all parents
		for(int i = 0; i < c.getParentCount(); i++) {
			p = (GraphCommit) c.getParent(i);
			p.setCumulatedX(p.getCumulatedX() + c.getXPos());
			p.setChildCount(p.getChildCount() + 1);
			p.setYPos(Math.max(p.getYPos(), c.getYPos() + 1));
		}

		// Calculate current commit's position
		if(c.getChildCount() == 0) {
			c.setXPos(0);
		}
		else {
			c.setXPos((c.getCumulatedX() + c.getChildCount() / 2) / c.getChildCount());
		}

		return c;
	}
}
